//
//  Trade.hpp
//  
//
//  Created by Tim Cabble on 11/03/2015.
//
//

#include "Trade.hpp"

Trade::Trade() {

}

Trade::Trade(std::string openD, std::string openT, float openV, int index){
    this->openDate = openD;
    this->openTime = openT;
    this->openValue = openV;
    this->size = 0;
    this->openStatus = true;
    this->tradeStartIndex = index;
    this->longSL = 0;
    this->longTP = 0;
    this->shortSL = 0;
    this->shortTP = 0;
    this->positionDrawdown = 0;
    this->spread = 0.15;
    this->commission = 5.8;
}


Trade::~Trade() {

}


bool Trade::isOpen(){
    return this->openStatus;
}

void Trade::setOpen(bool mode) {
    this->openStatus = mode;
}

std::string Trade::getDirection() {
    return this->direction;
}

float Trade::getPositionReturn(){
    return this->positionReturn;
}

float Trade::getPositionDrawdown() {
    return this->positionDrawdown;
}

float Trade::getPositionSize() {
    return this->size;
}

std::string Trade::getOpenDate() {
    return this->openDate;
}

std::string Trade::getOpenTime() {
    return this->openTime;
}

float Trade::getOpenValue() {
    return this->openValue;
}

float Trade::getCloseValue() {
    return this->closeValue;
}

float Trade::getLongTP() {
    return this->longTP;
}

float Trade::getLongSL() {
    return this->longSL;
}

float Trade::getShortTP() {
    return this->shortTP;
}

float Trade::getShortSL() {
    return this->shortSL;
}

std::string Trade::getCloseDate() {
    return this->closeDate;
}

std::string Trade::getCloseTime() {
    return this->closeTime;
}

void Trade::setDirection(std::string direction) {
    this->direction = direction;
}

void Trade::setPositionSize(float size) {
    this->size = size;
}

int Trade::getStartIndex() {
    return this->tradeStartIndex;
}

void Trade::setCloseDate(std::string closeD) {
    this->closeDate = closeD;
}

void Trade::setCloseTime(std::string closeT) {
    this->closeTime = closeT;
}

void Trade::setCloseValue(float value) {
    this->closeValue = value;
}

void Trade::setLongTP(float tp, Bar currentBar) {
    if (tp != 0) {
        this->longTP = currentBar.OPEN + tp + this->spread;
    } else {
        this->longTP = 0;
    }    
}

void Trade::setLongSL(float sl, Bar currentBar) {
    if (sl != 0) {
        this->longSL = currentBar.OPEN - sl - this->spread;
    } else {
        this->longSL = 0;
    }    
}

void Trade::setShortTP(float tp, Bar currentBar) {
    if (tp != 0) {
        this->shortTP = currentBar.OPEN - tp - this->spread;
    } else {
        this->shortTP = 0;
    }    
}

void Trade::setShortSL(float sl, Bar currentBar) {
    if (sl != 0) {
        this->shortSL = currentBar.OPEN + sl + this->spread;
    } else {
        this->shortSL = 0;
    }
    
}

void Trade::setPositionReturn(float value) {
    this->positionReturn = value * this->size * 50 - spread - commission;
}

void Trade::setPositionDrawdown(float value) {
    this->positionDrawdown = value;
}




