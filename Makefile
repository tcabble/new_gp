CC = g++

CFLAGS = -std=c++11 -g

GPCLASSES =                 \
	tester.cpp				\
	ChartData.cpp           \
    Evaluator.cpp           \
	GPTest.cpp 				\
	Indi.cpp 				\
	Run.cpp  				\
	FitnessFunction.cpp		\
	Trade.cpp				\
	Ledger.cpp				\
	TradeManager.cpp

OBJECTS = $(GPCLASSES:.cpp=.o)

test: $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -f *.o test