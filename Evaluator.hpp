//
//  Evaluator.hpp
//  
//
//  Created by Tim Cabble on 10/26/15.
//
//

#ifndef Evaluator_hpp
#define Evaluator_hpp

#include <stdio.h>
#include <cmath>
#include <memory>

#include "ChartData.hpp"
#include "Indi.hpp"
#include "Trade.hpp"

class Indi;

class Evaluator {
    
public:
    Evaluator(ChartData *historyData, int currentIndex);
    virtual ~Evaluator();

    ChartData *dataStore;

    int PC;

    int currentDataIndex;
    
    float run();
    
    void test();
    
    void reset(int index, Indi *indiv, Trade *newTrade, int mode);
    
    void setCurrentIndex(int index);
    
    void step();
    
    Bar getCurrentBar();
    
    Bar getBarAt(int index);

    void setDealSize(int mode);

    //Indi* getIndividual();

    //void setIndividual(Indi *indiv);
    
private:
    
    

    Indi *indiv;
    
    Trade *workingTrade;
    
    //int currentDataIndex;
    
    //int PC;
    
    float ma(int mode, int period, int shift);
    
    float macd(int fast, int slow, int signal, int shift, int line);
    
    float stoch(int fastk, int slowk, int d, int shift, int line);
    
    float rsi(int period, int shift);
    
    float mom(int period, int shift);
    
    float open(int index);
    
    float high(int index);
    
    float low(int index);
    
    float close(int index);
    
    float volume(int index);
};

#endif /* Evaluator_hpp */
