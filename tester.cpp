//
//  tester.cpp
//  
//
//  Created by Tim Cabble on 10/27/15.
//
//

#include <stdio.h>
#include "GPTest.hpp"
#include "Run.hpp"
#include "Evaluator.hpp"
#include "Trade.hpp"
#include "TradeManager.hpp"

int main(int argc, char *argv[]){
    ChartData dataStore;
    char *filename = argv[1];
    dataStore.loadData(filename);
    
    //dataStore.testData();
    
    Evaluator evaluator(&dataStore, 10);
    
    //evaluator.test();
    
    FitnessFunction theFunction;
    
    GPTest theTest(&theFunction, &evaluator, false);
    theTest.currentBestFitness = -1111111111111.0;
    
    Run newRun(&theTest);
    
    newRun.createRandomPopulation();

    newRun.evolve();
    
    std::cout << "---------------------------------------------------------------------------------" << std::endl;
    std::cout << "---------------------------------------------------------------------------------" << std::endl;
    std::cout << "---------------------------------------------------------------------------------" << std::endl;
    std::cout << "---------------------------------------------------------------------------------" << std::endl;
    std::cout << "---------------------------------------------------------------------------------" << std::endl;
    
    theFunction.calcValFitness(&evaluator, newRun.population.at(theTest.bestIndividualIndex));
    theFunction.calcTestFitness(&evaluator, newRun.population.at(theTest.bestIndividualIndex));

    newRun.population.at(theTest.bestIndividualIndex).getLedgers(0)->outputTrades();
    std::cout << std::endl;
    newRun.population.at(theTest.bestIndividualIndex).getLedgers(1)->outputTrades();
    std::cout << std::endl;
    newRun.population.at(theTest.bestIndividualIndex).getLedgers(2)->outputTrades();

    std::cout << std::endl;
    std::cout << "Fitness: " << newRun.population.at(theTest.bestIndividualIndex).fitness << std::endl;
    std::cout << "Return: " << newRun.population.at(theTest.bestIndividualIndex).getLedgers(0)->getTotalReturn() << std::endl;
    std::cout << "Train Drawdown: " << newRun.population.at(theTest.bestIndividualIndex).getLedgers(0)->getMaxDrawdown() << std::endl;
    std::cout << "Trade Distribution: " << newRun.population.at(theTest.bestIndividualIndex).getLedgers(0)->getTradeDistribution() << std::endl;
    std::cout << "Train Win Loss: " << newRun.population.at(theTest.bestIndividualIndex).getLedgers(0)->getWinLossRatio() << std::endl;
    std::cout << "Train HPRVariance: " << newRun.population.at(theTest.bestIndividualIndex).getLedgers(0)->getHPRVariance() << std::endl;
    std::cout << "Train StdDev: " << newRun.population.at(theTest.bestIndividualIndex).getLedgers(0)->getStdDev() << std::endl;
    std::cout << "Train EV: " << newRun.population.at(theTest.bestIndividualIndex).getLedgers(0)->getExpectedValue() << std::endl;
    std::cout << "Train fraction: " << newRun.population.at(theTest.bestIndividualIndex).getLedgers(0)->getMaxFraction() << std::endl;
    std::cout << "Train AHPR: " << newRun.population.at(theTest.bestIndividualIndex).getLedgers(0)->getAHPR() << std::endl;
    std::cout << "Train EGM: " << newRun.population.at(theTest.bestIndividualIndex).getLedgers(0)->getEGM() << std::endl;
    std::cout << "Val Fitness: " << newRun.population.at(theTest.bestIndividualIndex).valFitness << std::endl;
    std::cout << "Val Return: " << newRun.population.at(theTest.bestIndividualIndex).getLedgers(1)->getTotalReturn() << std::endl;
    std::cout << "Test Return: " << newRun.population.at(theTest.bestIndividualIndex).getLedgers(2)->getTotalReturn() << std::endl;
    std::cout << "Test Drawdown: " << newRun.population.at(theTest.bestIndividualIndex).getLedgers(2)->getMaxDrawdown() << std::endl;
    
    std::cout << "Done!" << std::endl;
 
    
}