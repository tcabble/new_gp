//
//  TradeManager.cpp
//  
//
//  Created by Tim Cabble on 11/03/2015.
//
//

#include "TradeManager.hpp"

TradeManager::TradeManager(Evaluator *eval, Indi *indiv, int startIndex, int stopIndex) {
	this->eval = eval;
    this->indiv = indiv;
    this->currentIndex = startIndex;
    this->stopIndex = stopIndex;
}

TradeManager::~TradeManager() {

}

Trade TradeManager::getCurrentTrade() {
    return this->currentTrade;
}

Evaluator* TradeManager::getEvaluator() {
    return this->eval;
}

Indi* TradeManager::getIndividual() {
    return this->indiv;
}

void TradeManager::runTrades(int mode) {
    //std::cout << "Running Trades mode " << mode << std::endl;
    this->eval->setCurrentIndex(this->currentIndex);
    while (this->currentIndex > this->stopIndex){
        //std::cout << std::endl;
        //std::cout << "Current Index: " << this->currentIndex << std::endl;
        //std::cout << "-----------------------Trade Start-------------------" << std::endl;
        //std::cout << this->eval->getCurrentBar().STARTTIME << std::endl;
        this->manageTrade(mode);
        //std::cout << "-------------------Trade Done---------------------" << std::endl;
        if (this->currentIndex == this->stopIndex) {
            //std::cout << this->eval->getBarAt(this->stopIndex).STARTTIME << std::endl;
            this->closeTrade(this->eval->getBarAt(this->stopIndex), 0);
            this->indiv->getLedgers(mode)->addTrade(this->currentTrade);
            //std::cout << "Added Trade end - size is: " << this->getIndividual()->getLedgers(mode)->tradeRegister.size() << std::endl;
            //this->indiv->printIndividual();
            //std::cout << std::endl;
        } else {
            //std::cout << this->eval->getCurrentBar().STARTTIME << std::endl;
        }
    }
    //std::cout << "+++++++++++++++++++++ Ledger Size: " << this->getIndividual()->getLedgers(mode)->tradeRegister.size() << " +++++++++++++++++" << std::endl;
    //std::cout << "&&&&&&&&&&&&&&&&&&&&& Total Return: " << this->getIndividual()->getLedgers(mode)->getTotalReturn() << " &&&&&&&&&&&&&&&&&&&" << std::endl;
}

void TradeManager::manageTrade(int mode) {
    //std::cout << "Mode " << mode << std::endl;
    this->newTrade();
    this->eval->reset(this->currentIndex, this->getIndividual(), &this->currentTrade, mode);
    //std::cout << "New Trade" << std::endl;
    //std::cout << this->eval->currentDataIndex << std::endl;
	this->dir = this->eval->run();
    if (dir == 1) {
        this->currentTrade.setDirection("Long");
    } else {
        this->currentTrade.setDirection("Short");
    }
    //std::cout << std::endl << "Dir: " << dir << std::endl;
	while (this->currentTrade.isOpen() && this->currentIndex > this->stopIndex) {
		this->step();
		//std::cout << "Dir: " << dir << std::endl;
	}
    if (this->currentIndex > this->stopIndex) {
        //std::cout << "adding trade" << std::endl;
        //std::cout << "direction: " << this->currentTrade.getDirection() << std::endl;
        this->indiv->getLedgers(mode)->addTrade(this->currentTrade);
        //std::cout << "Added Trade - size is: " << this->getIndividual()->getLedgers(mode)->tradeRegister.size() << std::endl;
        //this->indiv->printIndividual();
        //std::cout << std::endl;
    }
}


void TradeManager::step() {
    this->currentIndex--;
    if (this->currentIndex <= this->stopIndex) {
        return;
    }
    this->eval->step();
    Bar currentBar = this->eval->getCurrentBar();
    
    if(this->dir == 1) {
        if (currentBar.LOW <= this->currentTrade.getLongSL() && this->currentTrade.getLongSL() != 0) {
            this->closeTrade(currentBar, 1);
            return;
        }
        if (currentBar.HIGH >= this->currentTrade.getLongTP() && this->currentTrade.getLongTP() != 0) {
            this->closeTrade(currentBar, 2);
            return;
        }
    } else if(this->dir == -1) {
        if (currentBar.HIGH >= this->currentTrade.getShortSL() && this->currentTrade.getShortSL() != 0) {
            this->closeTrade(currentBar, 1);
            return;
        }
        if (currentBar.LOW <= this->currentTrade.getShortTP() && this->currentTrade.getShortTP() != 0) {
            this->closeTrade(currentBar, 2);
            return;
        }
    }
    if(this->dir == 1) {
        if (this->currentTrade.getPositionDrawdown() < (this->currentTrade.getOpenValue() - currentBar.LOW)) {
            this->currentTrade.setPositionDrawdown(this->currentTrade.getOpenValue() - currentBar.LOW);
        }   
    } else if(this->dir == -1) {
         if (this->currentTrade.getPositionDrawdown() < (currentBar.HIGH - this->currentTrade.getOpenValue())) {
            this->currentTrade.setPositionDrawdown(currentBar.HIGH - this->currentTrade.getOpenValue());
        }
    }
    float currentDir = this->eval->run();
    //std::cout << std::endl;
    if (currentDir != this->dir) {
        Bar closeBar = this->getEvaluator()->getBarAt(this->currentIndex - 1);
        this->closeTrade(closeBar, 0);
    }
}

void TradeManager::newTrade() {
    /*
    Bar currentBar = this->getEvaluator()->getCurrentBar();
    Trade newTrade(currentBar.STARTTIME, "0:00", currentBar.CLOSE, this->currentIndex);
    this->setTrade(newTrade);
     */
    Bar openBar = this->getEvaluator()->getBarAt(this->currentIndex - 1);
    Trade newTrade(openBar.STARTTIME, "0:00", openBar.OPEN, this->currentIndex - 1);
    this->setTrade(newTrade);
}

void TradeManager::setTrade(Trade newTrade) {
    this->currentTrade = newTrade;
}

void TradeManager::closeTrade(Bar curBar, int mode) {
    this->currentTrade.setOpen(false);
    this->currentTrade.setCloseDate(curBar.STARTTIME);
    switch (mode) {
        case 0: //No Stop or Target
            this->currentTrade.setCloseValue(curBar.OPEN);
            //std::cout << "Open Value: " << this->currentTrade.getOpenValue() << " Close Value: " << this->currentTrade.getCloseValue() << std::endl;
            if (this->dir == 1) {
                this->currentTrade.setPositionReturn(this->currentTrade.getCloseValue() - this->currentTrade.getOpenValue());
            } else {
                this->currentTrade.setPositionReturn(this->currentTrade.getOpenValue() - currentTrade.getCloseValue());
            }
            
            break;
        case 1: //stops out
            if (this->dir == 1) {
                this->currentTrade.setCloseValue(this->currentTrade.getLongSL());
                //std::cout << "Open Value: " << this->currentTrade.getOpenValue() << " Close Value: " << this->currentTrade.getCloseValue() << std::endl;
                this->currentTrade.setPositionReturn(this->currentTrade.getCloseValue() - this->currentTrade.getOpenValue());
            } else {
                this->currentTrade.setCloseValue(this->currentTrade.getShortSL());
                //std::cout << "Open Value: " << this->currentTrade.getOpenValue() << " Close Value: " << this->currentTrade.getCloseValue() << std::endl;
                this->currentTrade.setPositionReturn(this->currentTrade.getOpenValue() - currentTrade.getCloseValue());
            }
            break;
        case 2: //take profit
            if (this->dir == 1) {
                this->currentTrade.setCloseValue(this->currentTrade.getLongTP());
                //std::cout << "Open Value: " << this->currentTrade.getOpenValue() << " Close Value: " << this->currentTrade.getCloseValue() << std::endl;
                this->currentTrade.setPositionReturn(this->currentTrade.getCloseValue() - this->currentTrade.getOpenValue());
            } else {
                this->currentTrade.setCloseValue(this->currentTrade.getShortTP());
                //std::cout << "Open Value: " << this->currentTrade.getOpenValue() << " Close Value: " << this->currentTrade.getCloseValue() << std::endl;
                this->currentTrade.setPositionReturn(this->currentTrade.getOpenValue() - currentTrade.getCloseValue());
            }
            break;
    }

}









