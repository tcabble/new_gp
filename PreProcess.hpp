/*
 * PreProcess.hpp
 *
 *  Created on: Feb 17, 2014
 *      Author: Tim
 */

#ifndef PREPROCESS_HPP_
#define PREPROCESS_HPP_

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include "ChartData.hpp"

class PreProcess {

public:
	PreProcess();
	virtual ~PreProcess();

	int cases;
	int vars;
	int OOScases;
	int valCases;

	std::vector < std::vector<float> >  processData(int vars, int testCases, char *fileName);
	std::vector < std::vector<float> >  processDataClass(char *fileName, int inClass);


	void printData(std::vector<std::vector<float> > theVector);

};

#endif /* PREPROCESS_HPP_ */
