/*
 * Test.hpp
 *
 *  Created on: Feb 11, 2014
 *      Author: Tim
 *
 *  A singleton class used to hold the attributes of the GPMA test run
 */

#ifndef GPTEST_HPP_
#define GPTEST_HPP_

#include <vector>
#include <memory>
#include "FitnessFunction.hpp"
#include "Evaluator.hpp"


//forward declaration of necessary classes
class Evaluator;
class FitnessFunction;

//The class
class GPTest {
    
public:
	GPTest(FitnessFunction *theFunction, Evaluator *eval, bool wavelet);
	virtual ~GPTest();

	//Data member attributes
	std::string bestIndividual;
	FitnessFunction *function;

	int gen_wout_Imp;
	int gens;
	int populationSize;
	int maxLength;
	int minLength;
	int maxDepth;
	float permutation;
	float crossover_prob;
	int tourn_size;
	int RUNS;
	int currentRun;
	int runNo;
	int inputTestCases;
	int validationTestCases;
    
    float currentBestFitness;
    
    int bestIndividualIndex;

    int progsEvaled;


    std::vector<int> functions;
 
	//the funtion set
    static const int HEAD = 99;

	static const int IFELSE = 100;
	static const int AND = 101;
	static const int OR = 102;
	static const int XOR = 103;
	static const int LESS = 104;
	static const int GREAT = 105;
	static const int EQU = 106;
	static const int ADD = 107;
	static const int SUB = 108;
	static const int MUL = 109;
	static const int DIV = 110;
	static const int MIN = 111;
	static const int MAX = 112;
	static const int NEG = 113;
	static const int SIN = 114;
	static const int COS = 115;
	static const int TAN = 116;
	static const int LOG = 117;
    static const int MA = 118;
	static const int MACD = 119;
	static const int STOCH = 120;
	static const int RSI = 121;
	static const int MOM = 122;
	static const int OPEN = 123;
	static const int HIGH = 124;
	static const int LOW= 125;
	static const int CLOSE = 126;
	static const int VOLUME = 127;
	static const int FSET_START = ADD;
	static const int FSET_END = LOG;

	static const int TRADE_LONG = 90;
	static const int TRADE_SHORT = 91;
	static const int SL = 92;
	static const int TP = 93;
	static const int TRADESET_START = 90;
	static const int TRADESET_END = 93;



	//some helper functions are here

	int nextInt(int edge);
	float nextDbl();
	float nextDbl(float fmin, float fmax);
	void printInputs();
	int printIndividual(std::string indiv, int bufferCount);
    Evaluator* getEvaluator();

private:
	Evaluator *eval;

};

#endif /* GPTEST_HPP_ */
