//
//  Evaluator.cpp
//  
//
//  Created by Tim Cabble on 10/26/15.
//
//

#include "Evaluator.hpp"

Evaluator::Evaluator(ChartData *historyData, int currentIndex){
    this->PC = 0;
    this->dataStore = historyData;
    this->currentDataIndex = currentIndex;
}

Evaluator::~Evaluator() {
    
}

void Evaluator::reset(int index, Indi *indiv, Trade *newTrade, int mode) {
    this->PC = 0;
    this->currentDataIndex = index;
    this->indiv = indiv;
    this->workingTrade = newTrade;
    this->setDealSize(mode);

}

void Evaluator::setDealSize(int mode) {
    this->workingTrade->setPositionSize(this->indiv->getLedgers(mode)->getDealSize());
}

void Evaluator::setCurrentIndex(int index) {
    this->currentDataIndex = index;
}

void Evaluator::step() {
    this->currentDataIndex--;
    this->PC = 0;
}

Bar Evaluator::getCurrentBar() {
    return this->dataStore->getBar(this->currentDataIndex);
}

Bar Evaluator::getBarAt(int index) {
    return this->dataStore->getBar(index);
}

/*
Indi* Evaluator::getIndividual() {
    return this->indiv;
}

void Evaluator::setIndividual(Indi *indiv) {
    this->indiv = indiv;
}
*/

float Evaluator::run() {
    std::string program = this->indiv->program;
    //std::cout << this->PC << std::endl;

    char node = program[this->PC];

    //std::cout << (int)node << std::endl;
    
    this->PC++;

    int decision = 0;
    int then = 0;
    int elseThen = 0;
    int arg1 = 0, arg2 = 0, arg3 = 0, arg4 = 0, arg5 = 0;
    int a, b;
    double intpart;
    float fracpart;
    
    //std::cout << PC << std::endl;
    if (node < 90 ){//this->test->FSET_START
        //std::cout << "Returning: " << (int)node << std::endl;
        return ((int)node);
    }
    switch(node){
         case 99:
            //std::cout<<"HEAD ";
            decision = run();
            //std::cout<<"Should go LONG ";
            run();
            //std::cout<<"Should go SHORT ";
            run();
            return decision;
        case 100:
            //std::cout<<"IFELSE ";
            if (run() == 1) {
                then = run();
                run();
                return then;
            } else {
                run();
                elseThen = run();
                return elseThen;
            }
        case 101:
            //std::cout<<"AND ";
            a = run();
            b = run();
            if (a == 1 && b == 1) {
                return 1;
            } else {
                return -1;
            }
        case 102:
            //std::cout<<"OR ";
            a = run();
            b = run();
            if (a == 1 || b == 1) {
                return 1;
            } else {
                return -1;
            }
        case 103:
            //std::cout<<"XOR ";
            a = run();
            b = run();
            if ((a == 1 && b == -1) || (a == -1 && b == 1)) {
                return 1;
            } else {
                return -1;
            }
        case 104:
            //std::cout<<"LESS ";
            if (run() < run()) {
                return 1;
            } else {
                return -1;
            }
        case 105:
            //std::cout<<"GREAT ";
             if (run() > run()) {
                return 1;
            } else {
                return -1;
            }
        case 106:
            //std::cout<<"EQU ";
             if (run() == run()) {
                return 1;
            } else {
                return -1;
            }
        case 107:
            //std::cout<<"ADD ";
            return(run() + run());
        case 108:
            //std::cout<<"SUB ";
            return(run() - run());
        case 109:
            //std::cout<<"MUL ";
            return(run() * run());
        case 110:
            //std::cout<<"DIV ";
            {
            float num = run();
            float den = run();
            if (std::fabs(den)< 0.0000001) return (-1.0); //protected division
            else return (num/den);
            }
        case 111:
            //std::cout<<"MIN ";
            return(std::min(run(), run()));
        case 112:
            //std::cout<<"MAX ";
            return(std::max(run(), run()));
        case 113:
            //std::cout<<"NEG ";
            return -(run());
        case 114:
            //std::cout<<"SIN ";
            return (sin(run()));
        case 115:
            //std::cout<<"COS ";
            return (cos(run()));
        case 116:
            //std::cout<<"TAN ";
            return (tan(run()));
        case 117:
            //std::cout<<"LOG ";
            return (log(std::fabs(run())));
        case 118:
            //std::cout<<"MA ";
            arg1 = run();
            arg2 = run();
            arg3 = run();
            return this->ma(arg1, arg2, arg3);
        case 119:
            //std::cout<<"MACD ";
            arg1 = run();
            arg2 = run();
            arg3 = run();
            arg4 = run();
            arg5 = run();
            return this->macd(arg1, arg2, arg3, arg4, arg5);
        case 120:
            //std::cout<<"STOCH ";
            arg1 = run();
            arg2 = run();
            arg3 = run();
            arg4 = run();
            arg5 = run();
            return this->stoch(arg1, arg2, arg3, arg4, arg5);
        case 121:
            //std::cout<<"RSI ";
            arg1 = run();
            arg2 = run();
            return this->rsi(arg1, arg2);
        case 122:
            //std::cout<<"MOM ";
            arg1 = run();
            arg2 = run();
            return this->mom(arg1, arg2);
        case 123:
            //std::cout<<"OPEN ";
            return this->open(run());
        case 124:
            //std::cout<<"HIGH ";
            return this->high(run());
        case 125:
            //std::cout<<"LOW ";
            return this->low(run());
        case 126:
            //std::cout<<"CLOSE ";
            return this->close(run());
        case 127:
            //std::cout<<"VOLUME ";
            return this->volume(run());
        case 90:
            //std::cout<<"--LONG-- ";
            fracpart = run();
            //std::cout << "SL: " << fracpart << " ";
            this->workingTrade->setLongSL(fracpart, this->getBarAt(this->currentDataIndex - 1));//this->getBarAt(this->currentDataIndex - 1)
            fracpart = run();
            //std::cout << "TP: " << fracpart << " ";
            this->workingTrade->setLongTP(fracpart, this->getBarAt(this->currentDataIndex - 1));//this->getCurrentBar()
            return 1;
        case 91:
            //std::cout<<"--SHORT-- ";
            fracpart = run();
            //std::cout << "SL: " << fracpart << " ";
            this->workingTrade->setShortSL(fracpart, this->getBarAt(this->currentDataIndex - 1));//this->getCurrentBar()
            fracpart = run();
            //std::cout << "TP: " << fracpart << " ";
            this->workingTrade->setShortTP(fracpart, this->getBarAt(this->currentDataIndex - 1));//this->getCurrentBar()
            return 1;
        case 92:
            //std::cout<<"SL ";
            fracpart = modf(run(), &intpart);
            fracpart = std::fabs(fracpart) * this->dataStore->getAverageRange() * 2000;
            return fracpart;
        case 93:
            //std::cout<<"TP ";
            fracpart = modf(run(), &intpart);
            fracpart = std::fabs(fracpart) * this->dataStore->getAverageRange() * 2000;
            return fracpart;
    }
    return (0);
}

void Evaluator::test() {
    
    std::cout << "MA 0, 14, 0: " << this->ma(0, 14, 0) << std::endl;
    std::cout << "MA 1, 14, 0: " << this->ma(1, 14, 0) << std::endl;
    std::cout << "MA 0, 14, 10: " << this->ma(0, 14, 10) << std::endl;
    std::cout << "MA 1, 14, 10: " << this->ma(1, 14, 10) << std::endl;
    
    std::cout << "MACD 12, 26, 9, 0, 0: " << this->macd(12, 26, 9, 0, 0) << std::endl;
    std::cout << "MACD 12, 26, 9, 0, 1: " << this->macd(12, 26, 9, 0, 1) << std::endl;
    std::cout << "MACD 12, 26, 9, 10, 0: " << this->macd(12, 26, 9, 10, 0) << std::endl;
    std::cout << "MACD 12, 26, 9, 10, 1: " << this->macd(12, 26, 9, 10, 1) << std::endl;
    
    std::cout << "STOCH 7, 3, 3, 0, 0: " << this->stoch(7, 3, 3, 0, 0) << std::endl;
    std::cout << "STOCH 7, 3, 3, 0, 1: " << this->stoch(7, 3, 3, 0, 1) << std::endl;
    std::cout << "STOCH 7, 3, 3, 10, 0: " << this->stoch(7, 3, 3, 10, 0) << std::endl;
    std::cout << "STOCH 7, 3, 3, 10, 1: " << this->stoch(7, 3, 3, 10, 1) << std::endl;
    
    std::cout << "RSI 14, 0: " << this->rsi(14, 0) << std::endl;
    std::cout << "RSI 14, 10: " << this->rsi(14, 10) << std::endl;
    std::cout << "RSI 5, 0: " << this->rsi(5, 0) << std::endl;
    std::cout << "RSI 5, 10: " << this->rsi(5, 10) << std::endl;
    
    std::cout << "MOM 14, 0: " << this->mom(14, 0) << std::endl;
    std::cout << "MOM 14, 10: " << this->mom(14, 10) << std::endl;
    std::cout << "MOM 5, 0: " << this->mom(5, 0) << std::endl;
    std::cout << "MOM 5, 10: " << this->mom(5, 10) << std::endl;
    
    std::cout << "OPEN 5: " << this->open(5) << std::endl;
    std::cout << "HIGH 10: " << this->high(10) << std::endl;
    std::cout << "LOW 15: " << this->low(15) << std::endl;
    std::cout << "CLOSE 2: " << this->close(2) << std::endl;
    std::cout << "VOLUME 6: " << this->volume(6) << std::endl;
    
}

float Evaluator::ma(int mode, int period, int shift) {
    float result = 0;
    if (period < 2) {
        period = 15;
    }
    std::vector<Bar> calcData = this->dataStore->getBarsArray(this->currentDataIndex + shift, period);
    std::vector<Bar>::iterator it;
    float sum = 0;
    float multiplier = 0;
    float SMA = 0;
    switch(mode){
        case 0: //Simple
            
            for(it = calcData.begin(); it != calcData.end(); it++){
                sum += it->CLOSE;
            }
            result = sum / period;
            return result;
        case 1: //Exponential
            multiplier = 2 / (period + 1);
            SMA = this->ma(0, period, shift +1);
            result = (calcData.at(1).CLOSE - SMA) * multiplier + SMA;
            return result;
        case 2: //Weighted
            return result;
    }
    return -1;
}

float Evaluator::macd(int fast, int slow, int signal, int shift, int line) {
    float result = 0;
    if (fast < 2 || fast > 32) {
        fast = 12;
    }
    if (slow < 2 || slow >32) {
        slow = 26;
    }
    if (signal < 2 || signal > 20) {
        signal = 9;
    }
    
    switch (line){
        case 0:
            result = this->ma(1, fast, shift) - this->ma(1, slow, shift);
            return result;
        case 1:
            std::vector<float> data;
            std::vector<float>::iterator it;
            for (int i = shift + signal + 1; i > shift; --i) {
                it = data.begin();
                data.insert(it, this->macd(fast, slow, signal, i, 0));
            }
            float sum = 0;
            for(it = data.begin(); it != data.end(); it++){
                sum += *it;
            }
            float multiplier = 2 / (signal + 1);
            float SMA = sum / signal;
            result = (data.at(0) - SMA) * multiplier + SMA;
            return result;
    }
    return -1;
}

float Evaluator::stoch(int fastk, int slowk, int d, int shift, int line) {
    float result = 0;
    float highest = 0;
    float lowest = 2000.0;
    float SMA = 0;
    float MA = 0;
    float sum = 0;
    float longsum = 0;
    
    if (fastk < 3 || fastk > 20) {
        fastk = 14;
    }
    if (slowk < 3 || slowk > 15) {
        slowk = 3;
    }
    if (d < 3 || d > 15) {
        d = 3;
    }
    
    //std::cout << "---------" << fastk << " : " << slowk << " : " << d << " : " << shift << "---------" << std::endl;
    std::vector<Bar> calcData;
    std::vector<Bar>::iterator barBit;
    std::vector<float> data;
    std::vector<float>::iterator it;
    std::vector<float> slowdata;
    std::vector<float>::iterator bit;
    
    switch (line){
        case 0:
            
            for (int i = shift + slowk - 1; i >= shift; --i) {
                it = data.begin();
                data.insert(it, this->stoch(fastk, slowk, d, i, 2));
            }
            sum = 0;
            for(it = data.begin(); it != data.end(); ++it){
                sum += *it;
            }
            SMA = sum / slowk;
            return SMA;
        
        case 1:
            for (int i = shift + d - 1; i >= shift; --i) {
                bit = slowdata.begin();
                slowdata.insert(bit, this->stoch(fastk, slowk, d, i, 0));
                //std::cout << i << std::endl;
            }
            longsum = 0;
            for(bit = slowdata.begin(); bit != slowdata.end(); ++bit){
                longsum += *bit;
            }
            MA = longsum / d;
            return MA;
        
        case 2:
            calcData = this->dataStore->getBarsArray(this->currentDataIndex + shift, fastk);
            for (barBit = calcData.begin(); barBit != calcData.end(); ++barBit) {
                if (barBit->HIGH > highest) {
                    highest = barBit->HIGH;
                }
                if (barBit->LOW < lowest) {
                    lowest = barBit->LOW;
                }
            }
            result = (calcData.at(0).CLOSE - lowest) / (highest - lowest) * 100;
            return result;       
    }
    return -1;
}

float Evaluator::rsi(int period, int shift) {
    float result;
    
    if (period < 2) {
        period = 14;
    }
    
    float gains = 0;
    float loss = 0;
    float numGains = 0;
    float numLoss = 0;
    //std::cout << "-----------------------RSI " << period << " : " << shift << "---------------------" << std::endl;
    std::vector<Bar> calcData = this->dataStore->getBarsArray(this->currentDataIndex + shift, period);
    std::vector<Bar>::iterator it;
    for(it = calcData.begin(); it != calcData.end(); it++){
        if (it->CLOSE > it->OPEN) {
            gains += it->CLOSE - it->OPEN;
            numGains++;
        } else {
            loss += it->OPEN - it->CLOSE;
            numLoss++;
        }
    }
    if (gains != 0) {
        gains /= numGains;
    } else {
        gains = 1.0;
    }
    if (loss != 0) {
        loss /= numLoss;
    } else {
        loss = 1;
    }
    result = 100 - 100 / (1 + gains / loss);
    return result;
}

float Evaluator::mom(int period, int shift) {
    float result = 0;
    
    if (period < 2) {
        period = 14;
    }
    
    std::vector<Bar> calcData = this->dataStore->getBarsArray(this->currentDataIndex + shift, period);
    //std::cout << "----------------------- MOM " << period << " : " << shift << "---------------------" << std::endl;
    result = (calcData.at(0).CLOSE - calcData.back().CLOSE) / calcData.back().CLOSE * 100;
    return result;
}

float Evaluator::open(int index) {
    return this->dataStore->getBar(this->currentDataIndex + index).OPEN;
}

float Evaluator::high(int index) {
    return this->dataStore->getBar(this->currentDataIndex + index).HIGH;
}

float Evaluator::low(int index) {
    return this->dataStore->getBar(this->currentDataIndex + index).LOW;
}

float Evaluator::close(int index) {
    return this->dataStore->getBar(this->currentDataIndex + index).CLOSE;
}

float Evaluator::volume(int index) {
    return this->dataStore->getBar(this->currentDataIndex + index).VOLUME;
}












