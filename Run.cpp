/*
 * Run.cpp
 *
 *  Created on: Feb 11, 2014
 *      Author: Tim
 *
 * Implementation of the Run class
 */

#include "Run.hpp"
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>

//constructor
Run::Run(GPTest *theTest) {

	test = theTest;
	bestFitness = -99999999.0;
	bestLength = 0;

}

Run::~Run() {
	// TODO Auto-generated destructor stub
}


//Helper to grow individuals for the population
void Run::createRandomIdividual(Indi &indiv){
	
    int len = 0;
	std::string ind;
	//std::cout << "Random Indiv" << std::endl;
	len=grow(&ind,0,this->test->maxDepth, "root");

    
	while (len > this->test->maxLength)
	{	   
	   ind.clear();
	   len=grow(&ind, 0, this->test->maxDepth, "root");
	}
	growTrade(&ind, ind.size(), this->test->maxDepth, "long");
	growTrade(&ind, ind.size(), this->test->maxDepth, "short");

    for (int i = 0; i < 3; ++i) {
        Ledger newLedger;
        indiv.setLedger(newLedger, i);
    }
    indiv.program = ind;
    
    
}


//instantiates Indi objs and puts them in the population vector
void Run::createRandomPopulation(){
	std::cout << "creating population" << std::endl;
	
	for (int i = 0; i < test->populationSize; i++)
	{
		float dist = 0;
		while (dist < 0.15) {
			Indi newIndiv(this->test);
	        
			createRandomIdividual(newIndiv);
			std::cout << "------------------- Trying New Individual: " << i+1 << " -----------------" << std::endl;
			this->test->function->calcFitness(this->test->getEvaluator(), newIndiv);
			this->test->function->calcValFitness(this->test->getEvaluator(), newIndiv);
			//this->test->function->calcFitness(this->test->valInputs, newIndiv, 1);
			this->test->progsEvaled++;
			dist = newIndiv.getLedgers(0)->getTradeDistribution();
            //std::cout << "Distribution = " << dist << std::endl;
			if (dist > 0.05) {
				this->population.push_back(newIndiv);
				break;
			}
		}
                
		
	}
}


//the recursive function for randomly creating an individual program trade rule
int Run::grow(std::string *buffer,int pos,int depth, std::string type){

    int random = 0;
    char node = (char)test->nextInt(100);
    
    if (depth < 0) {
        return 1000;
    }

	if (type == "root") {
		node = (char)99;
        //std::cout << (int)node << std::endl;
		buffer->push_back(node);
		return  grow(buffer, pos+1, depth-1, "bool");
	} else if (type == "bool") {
		node= (char) test->nextInt(test->EQU - test->AND + 1) + test->AND;
		switch (node){
            
			case 101:
			case 102:
			case 103:
				buffer->push_back(node);
				return(grow(buffer, grow(buffer, pos+1, depth-1, "bool"), depth-1, "bool"));
			case 104:
			case 105:
			case 106:
				buffer->push_back(node);
				return(grow(buffer, grow(buffer, pos+1, depth-1, "any"), depth-1, "any"));
		}
		
	} else if (node < 50 || depth <= 0) {
	  	node = (char) test->nextInt(test->VOLUME - test->MA + 1) + test->MA;
		switch (node) {
            case 118:
                buffer->push_back(node);
                buffer->push_back((char)test->nextInt(2));
                buffer->push_back((char)(test->nextInt(40 - 3) + 3));
                buffer->push_back((char)test->nextInt(40));
                return (pos + 4);
            case 119:
                buffer->push_back(node);
                buffer->push_back((char)(test->nextInt(32 - 3) + 3));
                buffer->push_back((char)(test->nextInt(15 - 3) + 3));
                buffer->push_back((char)(test->nextInt(15 - 3) + 3));
                buffer->push_back((char)test->nextInt(40));
                buffer->push_back((char)test->nextInt(2));
                return (pos + 6);
            
            case 120:
                buffer->push_back(node);
                buffer->push_back((char)(test->nextInt(20 - 3) + 3));
                buffer->push_back((char)(test->nextInt(12 - 3) + 3));
                buffer->push_back((char)(test->nextInt(12 - 3) + 3));
                buffer->push_back((char)test->nextInt(40));
                buffer->push_back((char)test->nextInt(2));
                return (pos + 6);
			case 121:
			case 122:
				buffer->push_back(node);
				buffer->push_back((char)(test->nextInt(40 - 3) + 3));
				buffer->push_back((char)test->nextInt(40));
				return (pos + 3);
			case 123:
			case 124:
			case 125:
			case 126:
			case 127:
			  	buffer->push_back(node);
				buffer->push_back((char)test->nextInt(40));
				return (pos + 2);
		}

	} else {
		node = 0;
		while (true){
			node=(char) test->nextInt(test->LOG - test->IFELSE + 1)+test->IFELSE;
			if (node == 100 || (node > 106 && node < 118)) break;
		}
		
        //std::cout << "Func" << (int)node << std::endl;
		switch(node) {
			case 100:
                buffer->push_back(node);
                return(grow(buffer, grow(buffer,  grow(buffer, pos+1, depth-1, "bool"), depth-1, "any"), depth-1, "any"));
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
                buffer->push_back(node);
                return(grow(buffer, grow(buffer, pos+1, depth-1, "any"), depth-1, "any"));

            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
                buffer->push_back(node);
                return(grow(buffer, pos+1, depth-1, "any"));
		}
	}

	return (0);	
}


//the recursive function for randomly creating an individual program trade parameters
int Run::growTrade(std::string *buffer,int pos,int depth, std::string type){

	char node = (char)test->nextInt(100);


    if (type == "long") {
        node = (char)90;
        buffer->push_back(node);
        return(growTrade(buffer, growTrade(buffer, pos + 1, depth - 1, "SL"), depth - 1, "TP"));
    } else if (type == "short") {
        node = (char)91;
        buffer->push_back(node);
        return(growTrade(buffer, growTrade(buffer, pos + 1, depth - 1, "SL"), depth - 1, "TP"));
    } else if (type == "SL") {
        node = (char)92;
        buffer->push_back(node);
        return(growTrade(buffer, pos + 1, depth - 1, "any"));
    } else if (type == "TP") {
        node = (char)93;
        buffer->push_back(node);
        return(growTrade(buffer, pos + 1, depth - 1, "any"));
    } else if (node < 50 || depth <= 0) {
	  	node= (char) test->nextInt(test->VOLUME - test->MA + 1) + test->MA;
		switch (node) {
            case 118:
                buffer->push_back(node);
                buffer->push_back((char)test->nextInt(2));
                buffer->push_back((char)test->nextInt(40 - 3) + 3);
                buffer->push_back((char)test->nextInt(40));
                return (pos + 4);
            case 119:
                buffer->push_back(node);
                buffer->push_back((char)(test->nextInt(32 - 3) + 3));
                buffer->push_back((char)(test->nextInt(32 - 3) + 3));
                buffer->push_back((char)(test->nextInt(15 - 3) + 3));
                buffer->push_back((char)test->nextInt(40));
                buffer->push_back((char)test->nextInt(2));
                return (pos + 6);
                
            case 120:
                buffer->push_back(node);
                buffer->push_back((char)(test->nextInt(20 - 3) + 3));
                buffer->push_back((char)(test->nextInt(12 - 3) + 3));
                buffer->push_back((char)(test->nextInt(12 - 3) + 3));
                buffer->push_back((char)test->nextInt(40));
                buffer->push_back((char)test->nextInt(2));
                return (pos + 6);
            case 121:
            case 122:
                buffer->push_back(node);
                buffer->push_back((char)test->nextInt(40 - 3) + 3);
                buffer->push_back((char)test->nextInt(40));
                return (pos + 3);
			case 123:
			case 124:
			case 125:
			case 126:
			case 127:
			  	buffer->push_back(node);
				buffer->push_back((char)test->nextInt(40));
				return (pos + 2);
		}

	} else {
		node=(char) test->nextInt(test->FSET_END-test->FSET_START+1)+test->FSET_START;
        //std::cout << "Func " << (int)node << std::endl;
        switch(node) {
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
                buffer->push_back(node);
                return(growTrade(buffer, growTrade(buffer, pos+1, depth-1, "any"), depth-1, "any"));
                
            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
                buffer->push_back(node);
                return(growTrade(buffer, pos+1, depth-1, "any"));
        }
	}
	return (0);	
}


//Inorder traversal of the flattened program tree
int Run::traverse(std::string buffer, int bufferCount){
	//std::cout << "Buffer Count: " << bufferCount << std::endl;
	if (buffer[bufferCount] < 90) {
		return ++bufferCount;
	}

	if (buffer[bufferCount] > 117 && buffer[bufferCount] < 128) {
		switch ((int)buffer[bufferCount]) {
			case 118:
				++bufferCount;
	      		return (traverse(buffer, traverse(buffer, traverse(buffer, bufferCount))));
			case 119:
			case 120:
				++bufferCount;
	      		return (traverse(buffer, traverse(buffer, traverse(buffer, traverse(buffer, traverse(buffer, bufferCount))))));
			case 121:
			case 122:
				++bufferCount;
				return traverse(buffer, traverse(buffer, bufferCount));
			case 123:
			case 124:
			case 125:
			case 126:
			case 127:
				//return (bufferCount + 2);
			 	++bufferCount;
               return(traverse(buffer,bufferCount));
		}
	} else {
	   switch((int)buffer[bufferCount])
	   {
	      	case 99:
	      	case 100:
	      		++bufferCount;
	      		return (traverse(buffer, traverse(buffer, traverse(buffer, bufferCount))));
	      	case 101:
	      	case 102:
	      	case 103:
	      	case 104:
		  	case 105:
		  	case 106:
		  	case 107:
		  	case 108:
		  	case 109:
		  	case 110:
		  	case 111:
            case 112:
	    	  	++bufferCount;
	            return(traverse(buffer,traverse(buffer,bufferCount)));
	      	case 113:
	      	case 114:
	      	case 115:
	      	case 116:
            case 117:
               ++bufferCount;
               return(traverse(buffer,bufferCount));
            case 90:
            case 91:
               ++bufferCount;
               return(traverse(buffer,traverse(buffer,bufferCount)));
	      	case 92:
		  	case 93:
	    	  	++bufferCount;
	          	return(traverse(buffer,bufferCount));
	   	}
	}
	return(0);
}


//function to select a parent for crossover or mutation
int Run::tournament(){
	//std::cout << "Tourn" << std::endl;
	int i, competitor, best = this->test->nextInt(this->test->populationSize);
	double fbest = -100000000000000000000.0;

	for (i = 0; i < this->test->tourn_size; i++) {
	  competitor = this->test->nextInt(this->test->populationSize);
	  if(this->population.at(competitor).fitness > fbest) {
	     fbest = this->population.at(competitor).fitness;
	     best = competitor;
	  }
	}
	return(best);
}


//function to select an individual to kill off and replace
int Run::negative_tournament(){
	//std::cout << "Neg Tourn" << std::endl;
	int i, competitor, worst = this->test->nextInt(this->test->populationSize);
	double fworst = 10000000000000000000.0;

	for (i = 0; i < this->test->tourn_size; i++){ 
		competitor = this->test->nextInt(this->test->populationSize);
		if(this->population.at(competitor).fitness < fworst) {
		 fworst = this->population.at(competitor).fitness;
		 worst = competitor;
		}
	}
	return(worst);
}


//function to crossover two parents
void Run::crossover(int parent1, int parent2, int newindiv){
	//std::cout << "Cross" << std::endl;
    int xo1start,xo1end,xo2start,xo2end;
	std::string tempchild = "";

	std::vector<int> position;

	std::string indiv1 = this->population.at(parent1).program;

	std::string indiv2 = this->population.at(parent2).program;
	//get all the pieces

	//std::cout << "Pre Traverse" << std::endl;
	int len1 = traverse(indiv1,0);
	int len2 = traverse(indiv2,0);
	int lenoff = 0;

	xo1start = this->test->nextInt(len1-1)+1;

	//std::cout << "Start: " << xo1start << std::endl;

	if ((int)indiv1.at(xo1start) < 90 ) { // returns terminal
		//std::cout << "Terminal" << std::endl;
		//for (char it : indiv2) {
		//	if ((int)it < 90) {
		//		position.push_back((int)it);
		//	}
		//}
		//int location = this->test->nextInt(position.size());
		//xo2start = position.at(location);
		
		
		xo2start = this->test->nextInt(len2);
		//std::cout << (int)indiv2.at(xo2start) << std::endl;
        while ((int)indiv2.at(xo2start) > 89) {
			xo2start = this->test->nextInt(len2);
			//std::cout << (int)indiv2.at(xo2start) << std::endl;
        }
        //std::cout << "start 2: " << xo2start << std::endl;
    }

    if ((int)indiv1.at(xo1start) > 100 && (int)indiv1.at(xo1start) < 107 ) { //Returns Bool
    	//std::cout << "Bool" << std::endl;
		int node = this->test->nextInt(106 - 100 + 1) + 101;
		//std::cout << node << std::endl;
		xo2start = indiv2.find((char)node);
        while (xo2start == -1) {
        	node = this->test->nextInt(106 - 100 + 1) + 101;
			xo2start = indiv2.find((char)node);			
        }
        //std::cout << "start 2: " << xo2start << std::endl;
    }

    if ((int)indiv1.at(xo1start) == 100) {
    	//std::cout << "IF Else" << std::endl;
    	int node = this->test->nextInt(127 - 107 + 1) + 107;
    	//std::cout << node << std::endl;
    	xo2start = indiv2.find((char)node);
        while (xo2start == -1) {
        	node = this->test->nextInt(127 - 107 + 1) + 107;
        	//std::cout << node << std::endl;
			xo2start = indiv2.find((char)node);
        }
        //std::cout << "start 2: " << xo2start << std::endl;
    }

    if ((int)indiv1.at(xo1start) > 106 && (int)indiv1.at(xo1start) < 128) {
    	//std::cout << "Others" << std::endl;
    	int node = this->test->nextInt(127 - 107 + 1) + 107;
    	//std::cout << node << std::endl;
    	xo2start = indiv2.find((char)node);
         while (xo2start == -1) {
        	node = this->test->nextInt(127 - 107 + 1) + 107;
        	//std::cout << node << std::endl;
			xo2start = indiv2.find((char)node);
        }
        //std::cout << "start 2: " << xo2start << std::endl;
    }

    if ((int)indiv1.at(xo1start) > 89 && (int)indiv1.at(xo1start) < 94) {
    	//std::cout << "Trades" << std::endl;
    	
    	//std::cout << (int)indiv1.at(xo1start) << std::endl;
    	xo2start = indiv2.find(indiv1.at(xo1start));

    	if (xo2start == -1) {
    		exit (EXIT_FAILURE);
    	}
        
        //std::cout << "start 2: " << xo2start << std::endl;
    }
    //std::cout << "going to traverse 1" << std::endl;
	xo1end = traverse(indiv1,xo1start);
	//std::cout << "end 1: " << xo1end << std::endl;

	//xo2start = this->test->nextInt(len2);
	//std::cout << "Going to traverse 2" << std::endl;
	xo2end = traverse(indiv2,xo2start);
	//std::cout << "end 2: " << xo2end << std::endl;

	lenoff = xo1end-xo1start+xo2end-xo2start;
	//std::cout << "Make the child" << std::endl;
	//create the new ind from the strings above
	tempchild.insert(tempchild.begin(),indiv1.begin(),indiv1.begin()+xo1start);
	tempchild.insert(tempchild.begin()+xo1start,indiv2.begin()+xo2start,indiv2.begin()+xo2start+(xo2end-xo2start));
	tempchild.insert(tempchild.begin()+xo1start+(xo2end-xo2start),indiv1.begin()+xo1start+(xo1end-xo1start),indiv1.end());

	//lenoff = traverse(tempchild,0);
    
	//not to small or to big
	//if(lenoff < this->test->minLength || lenoff > this->test->maxLength)
	//	return;
    
	//set everything
	this->population.at(newindiv).program = tempchild;

	this->population.at(newindiv).valFitness = 0.0;

	//this->population.at(newindiv).printIndividual();
    //std::cout << "Evaluating" << std::endl;
	this->test->function->calcFitness(this->test->getEvaluator(), this->population.at(newindiv));
	this->test->function->calcValFitness(this->test->getEvaluator(), this->population.at(newindiv));
	this->test->progsEvaled++;

	
	//std::cout << std::endl;

	//this->population.at(newindiv).fitness = this->population.at(newindiv).fitness - fabs(this->population.at(newindiv).fitness - this->population.at(newindiv).valFitness);
}


//function to mutate an indiv
void Run::mutate(int parent, int newIndiv){
	
	int mutsite, i, len;

	std::string parentcopy;
	//get the random spot
	int newRand = this->test->nextInt(this->test->FSET_START );

	parentcopy = this->population.at(parent).program;

	len=parentcopy.size();
/*
	//find it and replace it
	for(i = 0;i < len; i++){
	  if(this->test->nextDbl() < this->test->permutation) {
		 mutsite=i;

		 if (parentcopy.at(mutsite) < this->test->FSET_START)
			parentcopy.at(mutsite) = (char) this->test->nextInt(this->test->varNumber);
		 else
			switch(parentcopy.at(mutsite)){
			   case 110:
			   case 111:
			   case 112:
			   case 113:
			   case 114:
			   case 115:
			   case 116:
			   case 117:
			   case 118:
					parentcopy.at(mutsite)=(char) (this->test->nextInt(this->test->FSET_END-this->test->FSET_START+1)+this->test->FSET_START);
					break;
			   case 119:
			   case 120:
			   case 121:
				   parentcopy.at(mutsite)=(char) (this->test->nextInt(this->test->FSET_END-this->test->COS+1)+this->test->COS);
			}
	  }

	  //set the new indiv
	  this->population.at(newIndiv)->program = parentcopy;
	  //this->test->function->calcFitness(this->test->inputs, this->population.at(newIndiv), 0);
	  //this->test->function->calcFitness(this->test->valInputs, this->population.at(newIndiv), 1);

	  this->population.at(newIndiv)->fitness = this->population.at(newIndiv)->fitness - fabs(this->population.at(newIndiv)->fitness - this->population.at(newIndiv)->valFitness);	  
	}
*/
}



//gather stats from the population		
void Run::stats(int gen){

	int best=this->test->populationSize/2;
	//int node_count = 0;
	//int ave_len;
	//bool flag = false;
	float fbestpop = this->population.at(best).fitness;
	//float favpop=0.0;

	for (int i = 0; i < this->test->populationSize; ++i) {
		//node_count += this->population.at(i)->program.length();
		//favpop += this->population.at(best)->fitness;
		if(this->population.at(i).fitness > fbestpop)
		{
            fbestpop = this->population.at(i).fitness;
            best = i;
		}
	}
    std::cout << "Best Fitness= " << this->population.at(best).fitness << std::endl;
    std::cout << "Programs Evaluated= " << this->test->progsEvaled << std::endl;
    this->test->bestIndividualIndex = best;

/*
	ave_len = node_count/this->test->populationSize;

	favpop/=this->test->populationSize;
*/
	if (this->test->currentBestFitness < fbestpop){
        this->test->currentBestFitness = fbestpop;
        std::cout << "--------------------------------Improvement  " << fbestpop <<"   Gen#  "<< this->test->gens << " ------------------------" << std::endl;
        this->test->bestIndividualIndex = best;
        this->test->gens = 0;
    } else {
        this->test->gens++;
    }
}



//function to produce the evolution of the indis
void Run::evolve() {
	std::cout << "evolving class: "  << std::endl;
	std::string p1;
	std::string p2;
	int gen=0,offspring,parent1,parent2,run=0;
	float newfit;

	//stats(this->test->gens);

   while(this->test->gens < this->test->gen_wout_Imp){
      for(int indivs = 0; indivs < this->test->populationSize; indivs++){//this->test->populationSize
    	  if(this->test->nextDbl() < this->test->crossover_prob){
        	parent1 = this->tournament();
			parent2 = this->tournament();
			p1 = this->population.at(parent1).program;
			p2 = this->population.at(parent2).program;
			while (p1 == p2){				
				parent2 = this->tournament();
				p2 = this->population.at(parent2).program;
			}
			//this->population.at(parent1).printIndividual();
			//std::cout << std::endl;
			//this->population.at(parent2).printIndividual();
			//std::cout << std::endl;
            offspring = negative_tournament();
            crossover(parent1, parent2, offspring);
         } 
        //else if (this->test->nextDbl() < this->test->permutation){
        //	parent1 = tournament();
        //    offspring = negative_tournament();
        //    mutate(parent1, offspring);
        // }
      }

      stats(this->test->gens);
   }

    //this->test->currentBestFitness = -11111111111.0;

    std::cout << "Pop Size= " << this->population.size() << std::endl;

}

void Run::printResults(int best){
/*
	std::cout << this->test->inputTestCases << std::endl;
	for (int i = 0; i < this->test->inputTestCases; i++){
		this->test->function->inputs = this->test->inputs->at(i);
		this->test->function->PC=0;
		std::cout << "we're at: " << i << " - " << this->test->inputs->at(i).at(this->test->varNumber) << " : " << round(this->test->function->run(this->population.at(best))) << std::endl;
	}
	std::cout << "we're out" << std::endl;
*/
}




