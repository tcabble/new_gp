//
//  ChartData.hpp
//  
//
//  Created by Tim Cabble on 10/26/15.
//
//

#ifndef ChartData_hpp
#define ChartData_hpp

#include <stdio.h>
#include <vector>
#include <sstream>
#include <iostream>
#include <cmath>
#include <utility>

struct Bar {

    float OPEN;
    float HIGH;
    float LOW;
    float CLOSE;
    float VOLUME;
    std::string STARTTIME;
};


class ChartData {

public:
    ChartData();
    virtual ~ChartData();
    
    Bar getBar(int index);
    
    std::vector<Bar> getBarsArray(int startIndex, int elementCount);
    
    void loadData(char *fileName);
    
    void testData();
    
    float getAverageRange();
    
    std::pair<int, int> getTrainingRange();
    std::pair<int, int> getValidationRange();
    std::pair<int, int> getTestingRange();
    
    
private:
    
    std::vector<Bar> chartBars;
    
    std::pair <int, int> trainingRange;
    std::pair <int, int> validationRange;
    std::pair <int, int> testingRange;
    
    float aveRange;
    
    void addBar(Bar newBar);
    
};

#endif /* ChartData_hpp */
