/*
 * Indi.hpp
 *
 *  Created on: Feb 11, 2014
 *      Author: Tim
 *
 * Indi class for the individuals in the population
 */

#ifndef INDI_HPP_
#define INDI_HPP_

#include <string>
#include <vector>
#include "GPTest.hpp"
#include "Ledger.hpp"

//forward declaration of necessary class
class GPTest;


class Indi {
public:
	//data member attributes
	Indi(GPTest *theTest);
	virtual ~Indi();
	int length;
	std::string program;
	double fitness;
	double equity;
	double valFitness;
	double valEquity;
	double OOSfitness;
	double OOSequity;

	GPTest *test;
    
    std::vector<Ledger> myLedgers;

	//Helper functions
	void printIndividual();
	int recPrintIndividual(std::string &program, int bufferCnt);
    void codeIndividual(int mode);
    int recCodeIndividualMT4(std::string &tempProgram, int bufferCnt);
	void setLedger(Ledger &aLedger, int index);
	Ledger* getLedgers(int index);

private:








};

#endif /* INDI_HPP_ */
