/*
 * PreProcess.cpp
 *
 *  Created on: Feb 17, 2014
 *      Author: Tim
 */

#include "PreProcess.hpp"
#include <sstream>

PreProcess::PreProcess(): cases(0), vars(0), OOScases(0), valCases(0) {

}

PreProcess::~PreProcess() {
	// TODO Auto-generated destructor stub
}

std::vector < std::vector<float> >  PreProcess::processData(int vars, int testCases, char *fileName){

	std::ifstream file(fileName);

	std::vector<std::vector<float> > inputs;
	float temp;
	std::streampos pos;
	std::string line;

	//std::cout << "got Here";

	for (int i = 0; i < testCases; i++){
		std::vector<float> tempVec;
		inputs.push_back(tempVec);
		for (int j = 0; j < vars+1; j++){
			if(j == 1){
				pos = file.tellg();
			}
			std::getline(file,line);
			std::stringstream stream(line);
			stream >> temp;
			inputs.at(i).push_back(temp);
		}
		file.seekg(pos);
	}
	file.close();
	return inputs;
}

std::vector < std::vector<float> >  PreProcess::processDataClass(char *fileName, int inClass){

	std::ifstream file(fileName);

	std::vector<std::vector<float> > inputs;
	float temp;
	std::string line;

	switch(inClass){

		case 0:{
			std::getline(file,line);
			cases ++;
			std::stringstream stream(line);

			while(stream >> temp){
				vars ++;
			}

			vars --;

			while(!file.eof()){
				std::getline(file,line);
				cases ++;
			}
			break;
		}

		case 1:
			while(!file.eof()){
				std::getline(file,line);
				valCases ++;
			}
			break;

		case 2:
			while(!file.eof()){
				std::getline(file,line);
				OOScases ++;
			}
			break;

	}

	std::cout << "test cases= " << cases << " vars= " << vars << std::endl;

	file.close();

	file.open(fileName, std::ifstream::in);

	for (int i = 0; i < cases-1; i++){
		std::vector<float> tempVec;
		inputs.push_back(tempVec);
		for (int j = 0; j < vars+1; j++){
			file >> temp;
			inputs.at(i).push_back(temp);
		}
	}
	file.close();
	return inputs;
}

void PreProcess::printData(std::vector<std::vector<float> > theVector){
	for (int i = 0; i < theVector.size(); i++){
		for (int j = 0; j < theVector.at(i).size(); j++){
			std::cout << theVector.at(i).at(j) << " ";
		}
		std::cout << std::endl;
	}
}

