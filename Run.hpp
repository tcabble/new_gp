/*
 * Run.hpp
 *
 *  Created on: Feb 11, 2014
 *      Author: Tim
 *
 * The Run class... does most of the work in the application
 */

#ifndef RUN_HPP_
#define RUN_HPP_

#include "Indi.hpp"
#include "GPTest.hpp"
#include "Ledger.hpp"
#include <string>

//forward declaration of classes
class GPTest;
class Indi;

//the class
class Run {
public:
	Run(GPTest *theTest);
	virtual ~Run();

	//data member attributes
	float bestFitness;
	int bestLength;
	int bestInd;

	//the population of Indi
	std::vector<Indi> population;
	GPTest *test;

	//the public helper functions
	void createRandomPopulation();
	void crossover(int parent1, int parent2, int newindiv);
	void mutate(int parent, int newindiv);
	void evolve();



private:
	//data mamber attributes
	int runNum;

	//the functions for the run ... the heart of the application
	int grow(std::string *buffer,int pos,int depth, std::string type);
	int growTrade(std::string *buffer,int pos,int depth, std::string type);
	void createRandomIdividual(Indi &indiv);
	int traverse(std::string buffer, int bufferCount);
	int tournament();
	int negative_tournament();
	void stats(int gen);
	void printResults(int best);
};

#endif /* RUN_HPP_ */
