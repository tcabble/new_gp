/*
 * FitnessFunction.cpp
 *
 *  Created on: Feb 17, 2014
 *      Author: Tim
 */

#include "FitnessFunction.hpp"
#include <string>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <memory>
#include <utility>

//constructor
FitnessFunction::FitnessFunction() {

}

FitnessFunction::~FitnessFunction() {

}

void FitnessFunction::calcFitness(Evaluator *eval, Indi &indiv) {
    //std::cout << "Calcing Fitness" << std::endl;
    std::pair<int, int> range = eval->dataStore->getTrainingRange();
    indiv.getLedgers(0)->clearTrades();
    TradeManager thisManager(eval, &indiv , range.first, range.second);
    thisManager.runTrades(0);
    //indiv.fitness = indiv.getLedgers(0)->getTotalReturn();
    if (indiv.getLedgers(0)->tradeRegister.size() < 25 || indiv.getLedgers(0)->getTradeDistribution() < 0.15 || indiv.getLedgers(0)->getMaxDrawdown() == 0) {
        indiv.fitness = -11111111111111.0;
    } else {
        //indiv.fitness = (indiv.getLedgers(0)->getTotalReturn() / indiv.getLedgers(0)->getMaxDrawdown() - (2 * indiv.getLedgers(0)->getStdDev()) - 
        //5.54 * indiv.program.size())  * indiv.getLedgers(0)->getWinLossRatio();
        //std::cout << "Drawdown: " << indiv.getLedgers(0)->getMaxDrawdown() << std::endl;
        //std::cout << "Relative Drawdown: " << indiv.getLedgers(0)->getMaxRelativeDrawdown() << std::endl;
        //indiv.fitness = indiv.getLedgers(0)->getEGM() * indiv.getLedgers(0)->getExpectedValue() - 5.54 * indiv.program.size();;
        
        if (indiv.getLedgers(0)->getMaxDrawdown() > 0) {
            indiv.fitness = indiv.getLedgers(0)->getTotalReturn() / indiv.getLedgers(0)->getMaxDrawdown() - 1.54 * indiv.program.size();
            //indiv.fitness = indiv.getLedgers(0)->getTotalReturn() / indiv.getLedgers(0)->getMaxDrawdown() * 50  * 
            //((1 + indiv.getLedgers(0)->getExpectedValue()) * indiv.getLedgers(0)->getEGM()) - 18.54 * indiv.program.size();
        } else {
            //indiv.fitness = indiv.getLedgers(0)->getTotalReturn() / 0.0001 - 5.54 * indiv.program.size();
        }
        
    }
}
    

void FitnessFunction::calcValFitness(Evaluator *eval, Indi &indiv) {
    //std::cout << "Calcing Val Fitness" << std::endl;
    std::pair<int, int> range = eval->dataStore->getValidationRange();
    indiv.getLedgers(1)->clearTrades();
    TradeManager thisManager(eval, &indiv, range.first, range.second);
    thisManager.runTrades(1);
    //float valReturn = indiv.getLedgers(1)->getTotalReturn();
    float valReturn = indiv.getLedgers(1)->getTotalReturn();
    if (valReturn < 0) {
    	indiv.fitness = -11111111111111.0;
    	indiv.valFitness = -11111111111111.0;
    } else {
        //indiv.valFitness = (indiv.getLedgers(1)->getTotalReturn() / indiv.getLedgers(1)->getMaxDrawdown() - (2 * indiv.getLedgers(1)->getStdDev()) - 
        //5.54 * indiv.program.size()) * indiv.getLedgers(1)->getWinLossRatio();
    	//indiv.valFitness = indiv.getLedgers(1)->getEGM() * indiv.getLedgers(1)->getExpectedValue() - 5.54 * indiv.program.size();;
        
        if (indiv.getLedgers(1)->getMaxDrawdown() > 0) {
            indiv.valFitness = indiv.getLedgers(1)->getTotalReturn() / indiv.getLedgers(1)->getMaxDrawdown()  - 1.54 * indiv.program.size();
            //indiv.valFitness = indiv.getLedgers(1)->getTotalReturn() / indiv.getLedgers(1)->getMaxDrawdown() * 50  *
            //(1 + indiv.getLedgers(1)->getEGM()) - 18.54 * indiv.program.size();
        } else {
            //indiv.valFitness = indiv.getLedgers(1)->getTotalReturn() / 0.0001 - 5.54 * indiv.program.size();
        }
        
    }    
}

void FitnessFunction::calcTestFitness(Evaluator *eval, Indi &indiv) {
    //std::cout << "Calcing Val Fitness" << std::endl;
    std::pair<int, int> range = eval->dataStore->getTestingRange();
    indiv.getLedgers(2)->clearTrades();
    TradeManager thisManager(eval, &indiv, range.first, range.second);
    thisManager.runTrades(2);
    //float valReturn = indiv.getLedgers(1)->getTotalReturn();
    //float valReturn = indiv.getLedgers(2)->getExpectedValue();
    //if (valReturn < 0) {
    //    indiv.fitness = -11111111111111.0;
    //    indiv.valFitness = -11111111111111.0;
    //} else {
    //    indiv.valFitness = indiv.getLedgers(2)->getTotalReturn();
    //    //indiv.valFitness = indiv.getLedgers(1)->getExpectedValue();
    //}    
}







