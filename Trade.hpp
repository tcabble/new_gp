//
//  Trade.hpp
//  
//
//  Created by Tim Cabble on 11/03/2015.
//
//

#ifndef Trade_hpp
#define Trade_hpp

#include <stdio.h>
#include <fstream>
#include "ChartData.hpp"

class Trade {

public:

	Trade();
	Trade(std::string openD, std::string openT, float openV, int index);
	virtual ~Trade();	

	void outputTrade(std::ofstream *fileName);

	bool isOpen();

	std::string getDirection();
	float getPositionReturn();
	float getPositionDrawdown();
	float getPositionSize();
    float getLongTP();
    float getLongSL();
    float getShortTP();
    float getShortSL();

	std::string getOpenDate();
	std::string getOpenTime();
    float getOpenValue();
    float getCloseValue();
	std::string getCloseDate();
	std::string getCloseTime();
	int getStartIndex();

	void setDirection(std::string direction);
	void setPositionSize(float size);
	void setCloseDate(std::string closeD);
	void setCloseTime(std::string closeT);
    void setCloseValue(float value);
    void setLongTP(float tp, Bar currentBar);
    void setLongSL(float sl, Bar currentBar);
    void setShortTP(float tp, Bar currentBar);
    void setShortSL(float sl, Bar currentBar);
    void setPositionReturn(float value);
    void setPositionDrawdown(float value);

	void setOpen(bool mode);

private:

	int tradeStartIndex;

	std::string direction;

	bool openStatus;
    
    float spread;
    float commission;
    
    float openValue;
    float closeValue;
    float size;
    
    float longTP;
    float longSL;
    float longDealSize;
    float shortTP;
    float shortSL;
    float shortDealSize;

	float positionReturn;
	float positionDrawdown;

	std::string openDate;
	std::string openTime;
	std::string closeDate;
	std::string closeTime;

};

#endif /* Trade_hpp */