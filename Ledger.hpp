//
//  Ledger.hpp
//  
//
//  Created by Tim Cabble on 11/03/2015.
//
//

#ifndef Ledger_hpp
#define Ledger_hpp

#include "Trade.hpp"
#include <vector>

class Ledger {

public:

	Ledger();
	virtual ~Ledger();

    std::vector<Trade> tradeRegister;


	float getTotalReturn();
	float getMaxDrawdown();
	float getMaxRelativeDrawdown();
	float getVariance();
	float getHPRVariance();
	float getStdDev();
	float getExpectedValue();
	float getTradeDistribution();
	float getWinLossRatio();
	float getMaxFraction();
	float getHPR(float theReturn);
	float getAHPR();
	float getEGM();
	float getProbabilityWin();
	float getProbabilityLoss();
	float getDealSize();
	Trade getTrade(int index);
	Trade getTradeByDate(std::string openDate);
	float getBalance();
	float getInitialBalance();

	void addTrade(Trade newTrade);
    void clearTrades();
    void outputTrades();

private:

	float balance;
	float initialBalance;
	float currentSize;

	//std::vector<Trade> tradeRegister;

};

#endif /* Ledger_hpp */








