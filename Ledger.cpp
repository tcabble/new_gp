//
//  Ledger.hpp
//  
//
//  Created by Tim Cabble on 11/03/2015.
//
//

#include "Ledger.hpp"
#include <iostream>
#include <fstream>

Ledger::Ledger() {
    this->initialBalance = 10000.00;
    this->balance = this->initialBalance;
    this->currentSize = 1.0;
}

Ledger::~Ledger() {

}

float Ledger::getTotalReturn() {
    float result = 0;
    for (Trade it : this->tradeRegister) {
        result += it.getPositionReturn();
    }
    return result;
}

float Ledger::getMaxDrawdown() {
    float peak = this->initialBalance;
    float trough = this->initialBalance;
    this->balance = this->initialBalance;

    float drawdown = 0;

    bool reset = false;

    for (Trade it : tradeRegister) {
        this->balance += it.getPositionReturn();
        if (this->balance >= peak) {
            peak = this->balance;
            reset = true;
            trough = this->balance;
        } else if (trough >= this->balance) {
            trough = this->balance;
            if (drawdown <= peak - trough) {
               drawdown = peak - trough;
            }
        } else {
            reset = false;
        }
    }
    return drawdown;
}

float Ledger::getMaxRelativeDrawdown() {
    float max = 0;
    for (Trade it : tradeRegister) {
        if (it.getPositionDrawdown() > max) {
            max = it.getPositionDrawdown();
        }
    }
    return max;
}

float Ledger::getStdDev() {
    return sqrt(getVariance());
}

float Ledger::getVariance() {
    float sum = 0;
    for (Trade it : tradeRegister) {
        sum += it.getPositionReturn();
    }

    float mean = sum / tradeRegister.size();

    //std::cout << "Mean: " << mean << std::endl; 

    float varSum = 0;

    for (Trade it : tradeRegister) {
        varSum += pow((it.getPositionReturn() - mean), 2);
    }

    //std::cout << "Var Sum: " << varSum << std::endl; 

    return varSum / tradeRegister.size();
}

float Ledger::getHPRVariance() {
    float sum = 0;
    for (Trade it : tradeRegister) {
        sum += getHPR(it.getPositionReturn());
    }

    float mean = sum / tradeRegister.size();

    //std::cout << "Mean: " << mean << std::endl; 

    float varSum = 0;

    for (Trade it : tradeRegister) {
        //std::cout << "Equ: " << getHPR(it.getPositionReturn()) - mean << std::endl;
        varSum += pow((getHPR(it.getPositionReturn()) - mean), 2);
    }

   // std::cout << "Var Sum: " << varSum << std::endl; 

    return varSum / tradeRegister.size();
}

float Ledger::getExpectedValue() {
    
    float ratio = getWinLossRatio();

    return ratio * getProbabilityWin() - getProbabilityLoss();
    
    /*
    float totalGain = 0;
    float gain = 0;
    float totalLoss = 0;
    float loss = 0;
    for (Trade it : this->tradeRegister) {
        if (it.getPositionReturn() > 0) {
            gain++;
            totalGain += it.getPositionReturn();
        } else {
            loss++;
            totalLoss += std::fabs(it.getPositionReturn());
        }
    }

    if (gain == 0) {
        return (-(totalLoss / loss) * loss / (gain + loss));
    } else if (loss == 0) {
        return (totalGain / gain) * (gain / (gain + loss));
    } else {
        return ((totalGain / gain) * (gain / (gain + loss)) - (totalLoss / loss) * (loss / (gain + loss)));
    }
    */   
}

float Ledger::getTradeDistribution() {
    int longs = 0;
    int shorts = 0;
    for (Trade it : this->tradeRegister) {
        if (it.getDirection() == "Long") {
            //std::cout << "Long" << std::endl;
            ++longs;
        } else {
            ++shorts;
            //std::cout << "Short" << std::endl;
        }
    }
    if (longs == 0 || shorts == 0) {
        return 0;
    } else {
        return (float)std::min(longs, shorts) / (float)std::max(longs, shorts);
    }
}

float Ledger::getWinLossRatio() {
    float winTally = 0;
    float lossTally = 0;
    int wins = 0;
    int loss = 0;
    for (Trade it : tradeRegister) {
        if (it.getPositionReturn() > 0) {
            ++wins;
            winTally += it.getPositionReturn();
        } else {
            ++loss;
            lossTally += std::fabs(it.getPositionReturn());
        }
    }
    if (wins == 0 || loss == 0) {
        return 0;
    } else {
        return (winTally / wins) / (lossTally / loss);
    }
}

float Ledger::getMaxFraction() {
    float winLoss = getWinLossRatio();
    float ev = getExpectedValue();

    if (winLoss == 0) {
        return 0;
    } else {
        return getExpectedValue() / getWinLossRatio();
    }
}

float Ledger::getHPR(float theReturn) {
    float maxLoss = 0;
    float maxF = getMaxFraction();
    for (Trade it : tradeRegister) {
        if (it.getPositionReturn() <= maxLoss) {
            maxLoss = it.getPositionReturn();
        }
    }
    return 1 + maxF * (-theReturn/maxLoss);
}

float Ledger::getAHPR() {
    float maxLoss = 0;
    float maxF = getMaxFraction();
    for (Trade it : tradeRegister) {
        if (it.getPositionReturn() <= maxLoss) {
            maxLoss = it.getPositionReturn();
        }
    }
    float sumHPR = 0;
    for (Trade it : tradeRegister) {
        sumHPR += (1 + maxF * (-it.getPositionReturn()/maxLoss));
    }
    //return sumHPR;
    return sumHPR / tradeRegister.size();
}

float Ledger::getEGM() {
    return sqrt(pow(getAHPR(), 2) - getHPRVariance());
}

float Ledger::getProbabilityWin() {
    float cnt = 0;
    for (Trade it : tradeRegister) {
        if (it.getPositionReturn() > 0) {
            ++cnt;
        }
    }
    return cnt / tradeRegister.size();
}

float Ledger::getProbabilityLoss() {
    float cnt = 0;
    for (Trade it : tradeRegister) {
        if (it.getPositionReturn() <= 0) {
            ++cnt;
        }
    }
    return cnt / tradeRegister.size();
}

float Ledger::getDealSize() {
    return 1.0;
    //std::cout << "Deal" << std::endl;
    if (this->tradeRegister.size() > 10 && this->getExpectedValue() > 0) {
        float F = this->getMaxFraction();
        float balance = this->getTotalReturn() + this->initialBalance;
        float maxLoss = 0;
        for (Trade it : tradeRegister) {
            if (it.getPositionReturn() <= maxLoss) {
                maxLoss = it.getPositionReturn();
            }
        }

        if (maxLoss <= 0) {
            float quotient = maxLoss / (-F);
            float dealSize = balance / quotient;
            this->currentSize = dealSize;
            //std::cout << "deal Done" << std::endl;
            return dealSize; 
        } else {
            //std::cout << "deal Done" << std::endl;
            return 1.0;
        }
    } else {
        //std::cout << "deal Done" << std::endl;
        return 1.0;
    }
}

void Ledger::addTrade(Trade newTrade) {
    this->tradeRegister.push_back(newTrade);
    this->balance += newTrade.getPositionReturn();
}

void Ledger::clearTrades() {
    if (this->tradeRegister.size() > 0) {
        this->tradeRegister.clear();
        this->currentSize = 1.0;
        this->balance = this->initialBalance;
    }
}

Trade Ledger::getTrade(int index) {
	return this->tradeRegister.at(index);
}
	
Trade Ledger::getTradeByDate(std::string openDate) {
	//std::vector<Trade*>::iterator it;
    int pos = 0;
    for (Trade it : tradeRegister) {
        if (it.getOpenDate() == openDate) {
            return this->tradeRegister.at(pos);
        }
        pos++;
    }
}

float Ledger::getBalance() {
    return this->balance;
}

float Ledger::getInitialBalance() {
    return this->initialBalance;
}

void Ledger::outputTrades() {
    std::cout << "---------------------------------------------------------Ledger--------------------------------------------------------------" << std::endl;
    std::cout << "Number\tDirection\tSize\tOpen Date\tClose Date\tOpen Price\tClose Price\tTP\t\tSL\t\tPosition Return" << std::endl;
    int cnt = 1;
    float TP = 0;
    float SL = 0;
    for (Trade it : tradeRegister) {
        if (it.getDirection() == "Long") {
            TP = it.getLongTP();
            SL = it.getLongSL();
        } else {
            TP = it.getShortTP();
            SL = it.getShortSL();
        }
        std::cout << cnt << "\t" << it.getDirection() << "\t\t" << it.getPositionSize() << "\t" << it.getOpenDate() << "\t" << it.getCloseDate() << "\t" << it.getOpenValue() << "\t\t" << it.getCloseValue()
            << "\t\t" << TP << "\t\t" << SL << "\t\t" << it.getPositionReturn() << std::endl;
        cnt++;
    }
    std::cout << "Total Return: " << this->getTotalReturn() << std::endl;
}