/*
 * Indi.cpp
 *
 *  Created on: Feb 11, 2014
 *      Author: Tim
 */

#include "Indi.hpp"
#include <iostream>
#include <vector>
#include <stdlib.h>

//constructor with pointer to the test instance of the test
Indi::Indi(GPTest *theTest){
	fitness = 0.0;
	equity = 0.0;
	test = theTest;
    this->myLedgers.resize(3);
}

Indi::~Indi() {
	//delete test;
}

void Indi::setLedger(Ledger &aLedger, int index){
    this->myLedgers.at(index) = aLedger;
}

Ledger* Indi::getLedgers(int index) {
    return &this->myLedgers.at(index);
}

//method to start the recursive printing of the individual
void Indi::printIndividual(){
    std::cout << "Printing Indi" << std::endl;
	recPrintIndividual(this->program, 0);
}

//the recursive function for traversal and print
int Indi::recPrintIndividual(std::string &tempProgram, int bufferCnt){
    //std::cout << tempProgram.size() << std::endl;
    
	int a1 = 0,a2 = 0, a3 = 0, a4 = 0, a5 = 0;
	if ((int)tempProgram[bufferCnt] < 90){//this->test->FSET_START){
		
        std::cout<< "val=" << (int)tempProgram[bufferCnt++] << ",";
		
		return(bufferCnt);
	}
    //std::cout << "In Here" << std::endl;
	switch((int)tempProgram[bufferCnt]){
        case 99:
            std::cout<<"HEAD ";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 100:
            std::cout<<"IFELSE ";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 101:
            std::cout<<"AND";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 102:
            std::cout<<"OR";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 103:
            std::cout<<"XOR";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 104:
            std::cout<<"LESS";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 105:
            std::cout<<"GREAT";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 106:
            std::cout<<"EQU";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 107:
            std::cout<<"ADD";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 108:
            std::cout<<"SUB";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 109:
            std::cout<<"MUL";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 110:
            std::cout<<"DIV";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 111:
            std::cout<<"MIN";
           std::cout << "(";
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           
           break;
        case 112:
            std::cout<<"MAX";
           std::cout << "(";
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           
           break;
        case 113:
            std::cout<<"NEG";
           std::cout << "(";
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           
           break;
        case 114:
            std::cout<<"SIN";
           std::cout << "(";
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 115:
            std::cout<<"COS";
           std::cout << "(";
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 116:
            std::cout<<"TAN";
           std::cout << "(";
           
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 117:
            std::cout<<"LOG";
           std::cout << "(";
           
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 118:
            std::cout<<"MA";
           std::cout << "(";
           
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 119:
            std::cout<<"MACD";
           std::cout << "(";
           
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 120:
            std::cout<<"STOCH";
           std::cout << "(";
           
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 121:
            std::cout<<"RSI";
           std::cout << "(";
           
           a1=recPrintIndividual(tempProgram,++bufferCnt);
           break;
        case 122:
            std::cout<<"MOM";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 123:
            std::cout<<"OPEN";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 124:
            std::cout<<"HIGH";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 125:
            std::cout<<"LOW";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 126:
            std::cout<<"CLOSE";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 127:
            std::cout<<"VOLUME";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 90:
            std::cout<<"LONG";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 91:
            std::cout<<"SHORT";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 92:
            std::cout<<"SL";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 93:
            std::cout<<"TP";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;


        }
        if((int)tempProgram[bufferCnt-1] > 100 && (int)tempProgram[bufferCnt-1] < 113) {
          a2=recPrintIndividual(tempProgram,a1);
          std::cout<<")";
          return(a2);
        } else if ((int)tempProgram[bufferCnt-1] > 120 && (int)tempProgram[bufferCnt-1] < 123) {
            a2=recPrintIndividual(tempProgram,a1);
            std::cout<<")";
            return(a2);
        } else if ((int)tempProgram[bufferCnt-1] > 129 && (int)tempProgram[bufferCnt-1] < 132) {
            a2=recPrintIndividual(tempProgram,a1);
            std::cout<<")";
            return(a2);
        } else if ((int)tempProgram[bufferCnt-1] > 98 && (int)tempProgram[bufferCnt-1] < 101) {
            a2=recPrintIndividual(tempProgram,a1);
            a3=recPrintIndividual(tempProgram,a2);
            std::cout<<")";
            return(a3);
        }   else if ((int)tempProgram[bufferCnt-1] > 89 && (int)tempProgram[bufferCnt-1] < 92) {
            a2=recPrintIndividual(tempProgram,a1);
            std::cout<<")";
            return(a2);
        } else if ((int)tempProgram[bufferCnt-1] == 118) {
            a2=recPrintIndividual(tempProgram,a1);
            a3=recPrintIndividual(tempProgram,a2);
            //a4=recPrintIndividual(tempProgram,a3);
            std::cout<<")";
            return(a3);
        } else if ((int)tempProgram[bufferCnt-1] == 119 || (int)tempProgram[bufferCnt-1] == 120) {
            a2=recPrintIndividual(tempProgram,a1);
            a3=recPrintIndividual(tempProgram,a2);
            a4=recPrintIndividual(tempProgram,a3);
            a5=recPrintIndividual(tempProgram,a4);
            std::cout<<")";
            return(a5);
        }  else if ((int)tempProgram[bufferCnt-1] < 90) {
           std::cout<<",";
          return(a1);
        } else {
           std::cout<<")";
          return(a1);
        }
	
}

//method to start the recursive printing of the individual
void Indi::codeIndividual(int mode){
    std::cout << "outputting code" << std::endl;
    switch (mode) {
        case 0:
            this->recCodeIndividualMT4(this->program, 0);
            break;
            
        default:
            break;
    }
}

//the recursive function for traversal and print
int Indi::recCodeIndividualMT4(std::string &tempProgram, int bufferCnt){
    //std::cout << tempProgram.size() << std::endl;
    
    int a1 = 0,a2 = 0, a3 = 0, a4 = 0, a5 = 0;
    if ((int)tempProgram[bufferCnt] < 90){//this->test->FSET_START){
        
        std::cout<< "val=" << (int)tempProgram[bufferCnt++] << ",";
        
        return(bufferCnt);
    }
    //std::cout << "In Here" << std::endl;
    switch((int)tempProgram[bufferCnt]){
        case 99:
            std::cout<<"HEAD ";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 100:
            std::cout<<"IFELSE ";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 101:
            std::cout<<"AND";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 102:
            std::cout<<"OR";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 103:
            std::cout<<"XOR";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 104:
            std::cout<<"LESS";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 105:
            std::cout<<"GREAT";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 106:
            std::cout<<"EQU";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 107:
            std::cout<<"ADD";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 108:
            std::cout<<"SUB";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 109:
            std::cout<<"MUL";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 110:
            std::cout<<"DIV";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 111:
            std::cout<<"MIN";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 112:
            std::cout<<"MAX";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 113:
            std::cout<<"NEG";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            
            break;
        case 114:
            std::cout<<"SIN";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 115:
            std::cout<<"COS";
            std::cout << "(";
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 116:
            std::cout<<"TAN";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 117:
            std::cout<<"LOG";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 118:
            std::cout<<"MA";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 119:
            std::cout<<"MACD";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 120:
            std::cout<<"STOCH";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 121:
            std::cout<<"RSI";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 122:
            std::cout<<"MOM";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 123:
            std::cout<<"OPEN";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 124:
            std::cout<<"HIGH";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 125:
            std::cout<<"LOW";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 126:
            std::cout<<"CLOSE";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 127:
            std::cout<<"VOLUME";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 90:
            std::cout<<"LONG";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 91:
            std::cout<<"SHORT";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 92:
            std::cout<<"SL";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
        case 93:
            std::cout<<"TP";
            std::cout << "(";
            
            a1=recPrintIndividual(tempProgram,++bufferCnt);
            break;
            
            
    }
    if((int)tempProgram[bufferCnt-1] > 100 && (int)tempProgram[bufferCnt-1] < 113) {
        a2=recPrintIndividual(tempProgram,a1);
        std::cout<<")";
        return(a2);
    } else if ((int)tempProgram[bufferCnt-1] > 120 && (int)tempProgram[bufferCnt-1] < 123) {
        a2=recPrintIndividual(tempProgram,a1);
        std::cout<<")";
        return(a2);
    } else if ((int)tempProgram[bufferCnt-1] > 129 && (int)tempProgram[bufferCnt-1] < 132) {
        a2=recPrintIndividual(tempProgram,a1);
        std::cout<<")";
        return(a2);
    } else if ((int)tempProgram[bufferCnt-1] > 98 && (int)tempProgram[bufferCnt-1] < 101) {
        a2=recPrintIndividual(tempProgram,a1);
        a3=recPrintIndividual(tempProgram,a2);
        std::cout<<")";
        return(a3);
    }   else if ((int)tempProgram[bufferCnt-1] > 89 && (int)tempProgram[bufferCnt-1] < 92) {
        a2=recPrintIndividual(tempProgram,a1);
        std::cout<<")";
        return(a2);
    } else if ((int)tempProgram[bufferCnt-1] == 118) {
        a2=recPrintIndividual(tempProgram,a1);
        a3=recPrintIndividual(tempProgram,a2);
        //a4=recPrintIndividual(tempProgram,a3);
        std::cout<<")";
        return(a3);
    } else if ((int)tempProgram[bufferCnt-1] == 119 || (int)tempProgram[bufferCnt-1] == 120) {
        a2=recPrintIndividual(tempProgram,a1);
        a3=recPrintIndividual(tempProgram,a2);
        a4=recPrintIndividual(tempProgram,a3);
        a5=recPrintIndividual(tempProgram,a4);
        std::cout<<")";
        return(a5);
    }  else if ((int)tempProgram[bufferCnt-1] < 90) {
        std::cout<<",";
        return(a1);
    } else {
        std::cout<<")";
        return(a1);
    }
    
}

