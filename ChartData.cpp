//
//  ChartData.cpp
//  
//
//  Created by Tim Cabble on 10/26/15.
//
//

#include "ChartData.hpp"
#include <vector>
#include <fstream>


ChartData::ChartData(){
    this->trainingRange = std::make_pair(8000, 7600);
    this->validationRange = std::make_pair(7599, 7100);
    this->testingRange = std::make_pair(7099, 0);
}

ChartData::~ChartData() {
    
}

void ChartData::loadData (char *fileName) {
    std::ifstream file(fileName);
    
    int bars = 0;
    
    float rangeSum = 0;

    std::string line;
    std::string innerLine;
    
    std::getline(file, line);
    std::stringstream numChartBars(line);
    numChartBars >> bars;
    
    for (int i = 0; i < bars; i++) {
        std::getline(file, line);
        std::istringstream barLine(line);
        Bar nextBar;
        barLine >> nextBar.OPEN;
        barLine >> nextBar.HIGH;
        barLine >> nextBar.LOW;
        barLine >> nextBar.CLOSE;
        barLine >> nextBar.VOLUME;
        barLine >> nextBar.STARTTIME;
        this->addBar(nextBar);
        rangeSum += std::fabs(nextBar.HIGH - nextBar.LOW);
    }
    rangeSum /= bars;
    this->aveRange = rangeSum / bars;
}

void ChartData::testData() {
    for (int i = 0; i < this->chartBars.size(); i++) {
        std::cout << "--------------New Bar " << i <<" ---------------" << std::endl;
        std::cout << "index: " << i << std::endl;
        std::cout << "Open: " << chartBars.at(i).OPEN << std::endl;
        std::cout << "High: " << chartBars.at(i).HIGH << std::endl;
        std::cout << "Low: " << chartBars.at(i).LOW << std::endl;
        std::cout << "Close: " << chartBars.at(i).CLOSE << std::endl;
        std::cout << "Volume: " << chartBars.at(i).VOLUME << std::endl;
        std::cout << "Time: " << chartBars.at(i).STARTTIME << std::endl;
    }
    
    std::cout << "Ave Range: " << this->aveRange << std::endl;
}

std::pair<int, int> ChartData::getTrainingRange() {
    return this->trainingRange;
}

std::pair<int, int> ChartData::getValidationRange() {
    return this->validationRange;
}

std::pair<int, int> ChartData::getTestingRange() {
    return this->testingRange;
}

float ChartData::getAverageRange() {
    return this->aveRange;
}

Bar ChartData::getBar(int index) {
    return this->chartBars.at(index);
}

std::vector<Bar> ChartData::getBarsArray(int startIndex, int elementCount) {
    std::vector<Bar> barsArray;
    for (int i = startIndex + elementCount; i > startIndex; i--) {
        barsArray.push_back(this->chartBars.at(i));
    }
    return barsArray;
}

void ChartData::addBar(Bar newBar) {
    //std::vector<Bar>::iterator it = this->chartBars.begin();
    //this->chartBars.insert(it, newBar);
    this->chartBars.push_back(newBar);
}


