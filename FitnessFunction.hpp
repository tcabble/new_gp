/*
 * FitnessFunction.hpp
 *
 *  Created on: Feb 17, 2014
 *      Author: Tim
 *
 * Class for the fitness function used in the test
 */

#ifndef FITNESSFUNCTION_HPP_
#define FITNESSFUNCTION_HPP_

#include "Indi.hpp"
#include "Evaluator.hpp"
//#include "GPTest.hpp"
#include "Run.hpp"
#include "TradeManager.hpp"
#include <vector>
#include <string>

//forward declaration of the necessary classes
//class Test;
class Indi;
class Evaluator;

class FitnessFunction {

public:
	//data member attributes
	FitnessFunction();
	virtual ~FitnessFunction();


	//the helper functions

	void calcFitness(Evaluator *eval, Indi &indiv);
    void calcValFitness(Evaluator *eval, Indi &indiv);
    void calcTestFitness(Evaluator *eval, Indi &indiv);




private:
	//some private members for future use
	enum functionType {REGRESSION, CLASSIFICATION, GAME};


};

#endif /* FITNESSFUNCTION_HPP_ */
